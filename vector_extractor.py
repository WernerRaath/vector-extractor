#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import shutil
import zipfile
import gzip
import tarfile
import uuid
from time import gmtime, strftime
import json
from osgeo import gdal, ogr, osr
import logging

logging.getLogger().setLevel(logging.INFO)

COMPRESSED_FILE = 'COMPRESSED_FILE'
UNCOMPRESSED_FILE = 'UNCOMPRESSED_FILE'

COMPRESSED_FORMAT = {'name': 'Compressed Files', 'extentions': ['tar',
                     'tar.gz', 'zip']}

SHAPEFILE_FORMAT = {
    'name': 'Shapefile',
    'extentions': ['dbf', 'shp', 'shx', 'cpg', 'prj', 'shp.xml', 'sbx', 'sbn'],
    'read_extention': 'shp',
}

TEXT_FORMAT = {
    'name': 'Text',
    'extentions': ['json', 'wkt', 'wkb']
}

SUPPORTED_GEOMETRY_TYPES = [
    'point',
    'linestring',
    'polygon',
    'multipoint',
    'multilinestring',
    'multipolygon',
    'geometrycollection',
]

SAVED_PROJECTION_CODE = 4326
SAVED_PROJECTION = 'ESPG:' + str(SAVED_PROJECTION_CODE)

SUPPORTED_COMPRESSED_FORMATS = [COMPRESSED_FORMAT]
SUPPORTED_UNCOMPRESSED_FORMATS = [SHAPEFILE_FORMAT]

def validate_file_extention(filename, supported_formats, file_category):
    result = None
    for supported_format in supported_formats:
        for supported_extention in supported_format['extentions']:
            if filename.find(supported_extention) > -1:
                result = {
                    'fullname': filename,
                    'filename': filename.split('.')[0],
                    'extention': supported_extention,
                    'category': file_category,
                    'format': supported_format,
                }
    return result

def validate_file_uncompressed(filename):
    return validate_file_extention(filename,
                                   SUPPORTED_UNCOMPRESSED_FORMATS,
                                   UNCOMPRESSED_FILE)

def bboxToEnvelopeForGeom(geom):
    (minX, maxX, minY, maxY) = geom.GetExtent()
    return f"POLYGON (({minX} {minY}, {maxX} {minY},{maxX} {maxY},{minX} {maxY},{minX} {minY}))"

def geoFileToLayersDict(file_location, silent=False):
    layers = []
    try:
        gdal.UseExceptions()
        src = gdal.OpenEx(file_location, gdal.OF_VECTOR)
        if src is not None:
            for i in range(src.GetLayerCount()):
                layer = src.GetLayer(i)
                layer_tag = "L[{}/{}] {}".format(i+1, src.GetLayerCount(), layer.GetName())
                
                layer.ResetReading()
                layer_def = layer.GetLayerDefn()
                l = {
                    'name': layer.GetName(),
                    'features': [],
                    'bad_features': [],
                    'union': None,
                    'attributes': []
                }

                y = 0
                features_wkt = []
                geometry_types = []
                for j, feat in enumerate(layer):
                    feature_tag = "F[{}/{}]".format(y+1, len(layer))
                    f = {'attr': {}, 'errors': []}
                    if not silent:
                        print( layer_tag, "|", feature_tag)

                    geom = feat.GetGeometryRef()
                    if geom is not None:
                        if not silent:
                            print( layer_tag, "|", feature_tag, "GEOM exists")
                        f['geom'] = str(geom)
                        features_wkt.append(f['geom'])

                        # if geom.IsValid():

                        geom_type = geom.GetGeometryName().lower()
                        if geom_type in SUPPORTED_GEOMETRY_TYPES:
                            if not silent:
                                print( layer_tag, "|", feature_tag, "GEOM supported")
                            source_srs = geom.GetSpatialReference()
                            target_srs = osr.SpatialReference()
                            target_srs.ImportFromEPSG(SAVED_PROJECTION_CODE)
                            transform = \
                                osr.CoordinateTransformation(source_srs,
                                                                                      target_srs)
                            geom.Transform(transform)
                            if not silent:
                                print( layer_tag, "|", feature_tag, "GEOM projected")
                            
                            f['geom'] = str(geom)
                            f['geom'] = f['geom'].replace(' 0)', ')')
                            f['geom_type'] = geom_type
                            if geom_type not in geometry_types:
                                geometry_types.append(geom_type)
                        else:
                            if not silent:
                                print( layer_tag, "|", feature_tag, "GEOM unsupported")
                            f['errors'].append({
                                'value': geom_type.upper(),
                                'error': 'Unsupported geometry type'
                            })
                    else:
                        f['errors'].append({ 'value': '', "error": 'Missing geometry'})
                    try:
                        for k in range(layer_def.GetFieldCount()):
                            field_defn = layer_def.GetFieldDefn(k)
                            field_name = field_defn.GetNameRef()
                            try:
                                attr_tag = "Attr[{}/{}]:{}".format(k+1, layer_def.GetFieldCount(), field_name)

                                if field_name not in l["attributes"]:
                                    l["attributes"].append(field_name)

                                if field_defn.GetType() == ogr.OFTInteger \
                                    or field_defn.GetType() == ogr.OFTInteger64:
                                    f['attr'][field_name] = \
                                        feat.GetFieldAsInteger64(k)
                                elif field_defn.GetType() == ogr.OFTReal:
                                    f['attr'][field_name] = \
                                        feat.GetFieldAsDouble(k)
                                elif field_defn.GetType() == ogr.OFTString:
                                    f['attr'][field_name] = \
                                        feat.GetFieldAsString(k)
                                else:
                                    f['attr'][field_name] = \
                                        feat.GetFieldAsString(k)
                                if j == 0:
                                    if not silent:
                                        print( layer_tag, "|", feature_tag, attr_tag)
                            except Exception as e:
                                if not silent:
                                    print(e)
                                f['errors'].append({
                                    "value": field_name,
                                    "error": str(e)
                                })
                    except Exception as e:
                        f['errors'].append({'value': "Error processing attributes", 'error': str(e)})
                    
                    if len(f.get('errors', [])) == 0:
                        l['features'].append(f)
                        if not silent:
                            print( layer_tag, "|", feature_tag, "SUCCESS")
                    else:
                        del f["geom"]
                        l['bad_features'].append(f)
                        if not silent:
                            print( layer_tag, "|", feature_tag, "FAIL\n", f['errors'])
                    
                    y += 1
                l['geometry_types'] = geometry_types

                if not silent:
                    print( layer_tag, "|", "[SUCCESS:{}    ERROR:{}]".format(
                      len(l['features']), len(l['bad_features']))
                    )

                layers.append(l)
            src = None
    except Exception as e:
        if not silent:
            print(e)
        return None
    return layers


def categorise_files(filename_list):
    result = {
        'valid_files': [],
        'invalid_files': [],
        'required_extentions': []
    }
    for file_info in filename_list:
        if type(file_info) is tarfile.TarInfo:
            filename = file_info.name
        elif type(file_info) is zipfile.ZipInfo:
            filename = file_info.filename
        validated_file = validate_file_uncompressed(filename)

        if validated_file is None:
            result['invalid_files'].append(filename)
        else:
            validated_file['fullname'] = validated_file['fullname']
            validated_file['info'] = file_info
            result['valid_files'].append(validated_file)

    return result

def decompress(path):
    filenames = []
    time_str = strftime('%Y%m%d%H%M%S', gmtime())
    extracted_output_path = os.path.splitext(path)[0] + "_" + time_str
    
    if path.find(".zip") > -1:
        compressed_file = zipfile.ZipFile(path, 'r')
        file_list = compressed_file.infolist()
    elif path.find(".tar") > -1:
        compressed_file = tarfile.open(path, 'r:')
        file_list = compressed_file.getmembers()
    elif path.find(".tar.gz") > -1:
        compressed_file = tarfile.open(path, 'r:gz')
        file_list = compressed_file.getmembers()
    
    validated_files = categorise_files(file_list)

    if len(validated_files['invalid_files']) > 0:
        print('invalid_files', validated_files['invalid_files'])

    if len(validated_files['required_extentions']) > 0:
        print('Missing required extentions!')

    if len(validated_files['valid_files']) > 0:
        filenames = [v_file['info'] for v_file in validated_files['valid_files']]
        compressed_file.extractall(extracted_output_path, filenames)

    compressed_file.close()

    return extracted_output_path

def add_if_true(dictionary, key, value):
    if value:
        dictionary['type'] = key

def process_path(path, files_only=True):
    list = []
    meta = process_path_recursive(path, list)
    if files_only:
        return list
    else:
        return meta


def process_path_recursive(path, _list=[]):
    is_directory = os.path.isdir(path)
    is_file = os.path.isfile(path)
    is_valid = False
    is_compressed_file = False
    is_uncompressed_file = False

    _metadata = {
        'path': path,
    }

    if is_file:
        has_compressed_extention = len([ext for ext in COMPRESSED_FORMAT['extentions'] if path.find(ext) > -1]) > 0
        is_compressed_file = has_compressed_extention
        is_uncompressed_file = not has_compressed_extention

        if is_uncompressed_file:
            for _format in SUPPORTED_UNCOMPRESSED_FORMATS:
                extentions = _format["extentions"]
                if _format.get("read_extention", None) is not None:
                    extentions = [_format['read_extention']]
                for ext in extentions:
                    if path.find(ext) > -1:
                        is_valid = True
                        break;
        elif is_compressed_file:
             is_valid = True

        if is_file and is_valid:
            layers = geoFileToLayersDict(path)
            if layers is not None:
                _metadata["layers"] = layers
            else:
                is_valid = False
    else:
        is_valid = True
    
    _metadata['is_valid'] = is_valid    
    add_if_true(_metadata, "directory", is_directory)
    add_if_true(_metadata, "compressed_file", is_compressed_file)
    add_if_true(_metadata, "uncompressed_file", is_uncompressed_file)

    if is_file and is_uncompressed_file:        
        _list.append(_metadata)

    if not is_valid:
        return _metadata

    if is_compressed_file:
        path = decompress(path)
        is_directory = True

    if is_directory:
        _metadata["children"] = []
        for ch in os.listdir(path):
            meta = process_path_recursive(os.path.join(path, ch), _list)
            if meta:
                _metadata["children"].append(meta)

    return _metadata
